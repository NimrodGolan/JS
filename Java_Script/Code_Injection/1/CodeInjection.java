import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

public class CodeInjection extends HttpServlet{
	
	public void doGet(HttpServletRequest req, HttpServletResponse res)
    throws ServletException, IOException
{
    res.setContentType("text/html");
    ServletOutputStream out = res.getOutputStream();
    out.println("<HTML><HEAD><TITLE>Test</TITLE></HEAD><BODY><blockquote><pre>");

            String fname = req.getParameter("fname");
            String msg = req.getParameter("msg");
 //interactive input          
            String testServerName = "localhost" +fname +msg;
		try {
//code injection
			Class t = Class.forName("resources/" + testServerName + ".js");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
