import java.io.IOException;
import java.text.AttributedCharacterIterator.Attribute;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.jar.Attributes;

import javax.naming.Name;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

class ldap extends HttpServlet{
	public void doGet(HttpServletRequest req, HttpServletResponse res)
    throws ServletException, IOException
{
    res.setContentType("text/html");
    ServletOutputStream out = res.getOutputStream();
    out.println("<HTML><HEAD><TITLE>Test</TITLE></HEAD><BODY><blockquote><pre>");

            String fname = req.getParameter("fname");
            String msg = req.getParameter("msg");
            
            String testServerName = "localhost" +fname +msg;
LinkedHashSet<String> roleNames = new LinkedHashSet<String>();

SearchControls searchCtls = new SearchControls();

searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

String userPrincipalName = testServerName;

Object principalSuffix;
if (principalSuffix != null) {

userPrincipalName += principalSuffix;

}

      String searchFilter = "(&(objectClass=*)(userPrincipalName="+ userPrincipalName + "))";
      Name searchBase;
	DirContext ldapContext;
	NamingEnumeration answer = ldapContext.search(searchBase,searchFilter,searchCtls);

while (answer.hasMoreElements()) {

SearchResult sr = (SearchResult) answer.next();

if (log.isDebugEnabled()) {

log.debug("Retrieving group names for user ["+ sr.getName() + "]");

}

javax.naming.directory.Attributes attrs = sr.getAttributes();

if (attrs != null) {

NamingEnumeration ae = attrs.getAll();

while (ae.hasMore()) {

Attribute attr = (Attribute) ae.next();

if (((Object) attr).getID().equals("memberOf")) {

Collection<String>groupNames = LdapUtils.getAllAttributeValues(attr);

if (log.isDebugEnabled()) {

log.debug("Groups found for user ["+ username + "]:"+ groupNames);

}

Collection<String>rolesForGroups = getRoleNamesForGroups(groupNames);

roleNames.addAll(rolesForGroups);

}

}

}

}

return roleNames;

}



protected Collection<String>getRoleNamesForGroups(Collection<String>groupNames) {

Set<String>roleNames = new HashSet<String>(groupNames.size());

if (groupRolesMap != null) {

for (String groupName:groupNames) {

String strRoleNames = groupRolesMap.get(groupName);

if (strRoleNames != null) {

for (String roleName:strRoleNames.split(ROLE_NAMES_DELIMETER)) {

if (log.isDebugEnabled()) {

log.debug("User is member of group ["+ groupName + "] so adding role ["+ roleName + "]");

}

roleNames.add(roleName);

}

}

}

}

return roleNames;

}

}
